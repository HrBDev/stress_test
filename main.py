import logging

from selenium import webdriver
from selenium.webdriver.common.by import By
from xvfbwrapper import Xvfb

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)
logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()

with Xvfb() as xvfb:
    driver = webdriver.Firefox()
    driver.get('https://mconf.github.io/api-mate/')
    logger.info('Opened api mate...')
    # Fill url
    server_element = driver.find_element_by_id('input-custom-server-url')
    server_element.clear()
    server_element.send_keys('http://test.bbb-conf.ir/bigbluebutton/')

    # Fill secret
    secret_element = driver.find_element_by_id('input-custom-server-salt')
    secret_element.clear()
    secret_element.send_keys('testxdNwldjuikC1cFr271xhJ5yKrYJ4dF6EY0PAbKHzg')

    # Create #i meetings
    for i in range(10):
        meeting_id = driver.find_element(by=By.XPATH, value='//*[@id="input-mid"]')
        meeting_id.clear()
        meeting_id.send_keys('random-{}'.format(i))
        links = driver.find_element_by_class_name('api-mate-links')
        divs = links.find_elements(value='api-mate-link-wrapper', by=By.CLASS_NAME)
        get_button = divs[1].find_element_by_class_name('label')
        get_button.click()
        logger.info('Creating meeting #{}...'.format(i))
        # Join each meeting j times
        for j in range(3):
            links = driver.find_element_by_class_name('api-mate-links')
            divs = links.find_elements(value='api-mate-link-wrapper', by=By.CLASS_NAME)
            join_button = divs[3].find_element_by_class_name('label')
            join_button.click()
            logger.info('Joining meeting #{}...'.format(i))

    print('Press any key to terminate!')
    x = input()
