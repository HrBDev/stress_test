# Requirements
1. xvfb
2. firefox
3. geckodriver
    ```bash
    wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz
    tar -xvzf geckodriver*
    chmod +x geckodriver
    export PATH=$PATH:/path-to-extracted-file/.
    ```
4. `pip install -r requirements.txt`    